package com.alexander.mazanov.ImproveTest;

import com.alexander.mazanov.ImproveTest.dto.action.ActionDescriptionDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

@ActiveProfiles("empty-db")
class EmptyDbTest extends BaseApplicationTest {

    @Test
    void getEmptyActionListTest() {
        ResponseEntity<List<ActionDescriptionDto>> responseEntity = restTemplate.exchange("/action", HttpMethod.GET, null, ACTION_LIST_RESPONSE_TYPE);

        Assertions.assertThat(responseEntity.getBody()).isEmpty();
    }
}
