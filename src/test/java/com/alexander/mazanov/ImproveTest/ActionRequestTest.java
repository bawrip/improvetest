package com.alexander.mazanov.ImproveTest;

import com.alexander.mazanov.ImproveTest.dto.action.ActionDescriptionDto;
import com.alexander.mazanov.ImproveTest.dto.action.ActionDto;
import com.alexander.mazanov.ImproveTest.dto.action.CreateActionDto;
import com.alexander.mazanov.ImproveTest.dto.product.ProductDescriptionDto;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

@Slf4j
@ActiveProfiles("test")
class ActionRequestTest extends BaseApplicationTest {

    @Test
    @SneakyThrows
    void getActionListTest() {
        ResponseEntity<List<ActionDescriptionDto>> responseEntity = restTemplate.exchange("/action", HttpMethod.GET, null, ACTION_LIST_RESPONSE_TYPE);

        List<ActionDescriptionDto> actions = responseEntity.getBody();
        actions = filterActionSearchInformation(actions, "search_");

        List<ActionDescriptionDto> referenceAction = loadFromJson("action/list/simple.list.json", ACTION_DESCRIPTION_DTO_TYPE);

        Assertions.assertThat(actions)
                .usingRecursiveFieldByFieldElementComparator().isEqualTo(referenceAction);
    }

    @SneakyThrows
    @ParameterizedTest
    @CsvSource({
            "/action/1?filter=, action/dto/filter/two.product.json",
            "/action/1?filter=d2, action/dto/filter/one.product.json",
            "/action/1?filter=ww, action/dto/filter/zero.product.json"
    })
    void getActionByIdWithParameters(String requestUtl, String referenceFilename) {
        ResponseEntity<ActionDto> responseEntity = restTemplate.exchange(requestUtl, HttpMethod.GET, null, ActionDto.class);

        ActionDto responseActionDto = responseEntity.getBody();
        List<ProductDescriptionDto> filteredProducts = responseActionDto.getProducts();
        filteredProducts = filterProductSearchInformation(filteredProducts, "search_");
        responseActionDto.setProducts(filteredProducts);

        ActionDto referenceActionDto = loadFromJson(referenceFilename, ActionDto.class);

        Assertions.assertThat(responseActionDto).usingRecursiveComparison().isEqualTo(referenceActionDto);

    }

    @Test
    void saveActionTest() {
        CreateActionDto createActionDto = new CreateActionDto();
        createActionDto.setName("create_test_action1");
        createActionDto.setCount(10);

        HttpEntity<CreateActionDto> httpEntity = new HttpEntity<>(createActionDto);
        ResponseEntity<Long> responseEntity = restTemplate.exchange("/action", HttpMethod.POST, httpEntity, Long.class);

        Long actionId = responseEntity.getBody();
        ResponseEntity<ActionDescriptionDto> actionDescriptionDtoByResponseId = restTemplate.exchange("/action/get/" + actionId, HttpMethod.GET, null, ActionDescriptionDto.class);

        ActionDescriptionDto referenceProductDescriptionDto = loadFromJson("action/dto/create/action.one.json", ActionDescriptionDto.class);

        Assertions.assertThat(actionDescriptionDtoByResponseId.getBody())
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(referenceProductDescriptionDto);
    }
}
