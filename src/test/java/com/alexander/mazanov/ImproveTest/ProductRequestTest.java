package com.alexander.mazanov.ImproveTest;

import com.alexander.mazanov.ImproveTest.dto.action.ActionDto;
import com.alexander.mazanov.ImproveTest.dto.action.CreateActionDto;
import com.alexander.mazanov.ImproveTest.dto.product.ProductDescriptionDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
public class ProductRequestTest extends BaseApplicationTest {

    @Test
    void decreaseProductCountAndGetActionById() {
        ResponseEntity<ActionDto> responseEntity = restTemplate.exchange("/product/4?actionId=5", HttpMethod.GET, null, ActionDto.class);

        ActionDto responseActionDto = responseEntity.getBody();
        responseActionDto.setProducts(filterProductSearchInformation(responseActionDto.getProducts(), "prod4"));

        ActionDto referenceActionDto = loadFromJson("action/dto/product/decrease/decrease.product.one.json", ActionDto.class);

        Assertions.assertThat(responseActionDto).usingRecursiveComparison().isEqualTo(referenceActionDto);
    }

    @ParameterizedTest
    @CsvSource({
        "/product/5?actionId=6," +
                "/action/6?filter=prod5," +
                "action/dto/product/decrease/decrease.product.two.json," +
                "Error. No gifts of this kind",
        "/product/6?actionId=7," +
                "/action/7?filter=prod6," +
                "action/dto/product/decrease/decrease.product.three.json," +
                "Error. No gifts for this promotion"
    })
    void whenOneCounterZero_thanAnotherCounterNotDecrease(String testRequestUrl, String requestForCheckResult, String referenceFilename, String expectedMessage) {
        ResponseEntity<String> responseEntityWithException = restTemplate.exchange(testRequestUrl, HttpMethod.GET, null, String.class);

        ResponseEntity<ActionDto> responseEntity = restTemplate.exchange(requestForCheckResult, HttpMethod.GET, null, ActionDto.class);

        ActionDto responseActionDto = responseEntity.getBody();
        ActionDto referenceActionDto = loadFromJson(referenceFilename, ActionDto.class);

        Assertions.assertThat(responseEntityWithException.getBody()).isEqualTo(expectedMessage);
        Assertions.assertThat(responseActionDto).usingRecursiveComparison().isEqualTo(referenceActionDto);
    }

    @Test
    void saveProductTest() {
        CreateActionDto createActionDto = new CreateActionDto();
        createActionDto.setName("create_test_prod1");
        createActionDto.setCount(5);

        HttpEntity<CreateActionDto> httpEntity = new HttpEntity<>(createActionDto);

        ResponseEntity<Long> responseEntity = restTemplate.exchange("/product", HttpMethod.POST, httpEntity, Long.class);
        Long productId = responseEntity.getBody();

        ResponseEntity<ProductDescriptionDto> productDescriptionDtoByResponseId = restTemplate.exchange("/product/get/" + productId, HttpMethod.GET, null, ProductDescriptionDto.class);
        ProductDescriptionDto referenceProductDescriptionDto = loadFromJson("action/dto/create/product.one.json", ProductDescriptionDto.class);

        Assertions.assertThat(productDescriptionDtoByResponseId.getBody())
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(referenceProductDescriptionDto);
    }
}
