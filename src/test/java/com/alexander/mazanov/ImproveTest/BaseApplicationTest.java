package com.alexander.mazanov.ImproveTest;

import com.alexander.mazanov.ImproveTest.dto.action.ActionDescriptionDto;
import com.alexander.mazanov.ImproveTest.dto.product.ProductDescriptionDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;

import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BaseApplicationTest {
    final ParameterizedTypeReference<List<ActionDescriptionDto>> ACTION_LIST_RESPONSE_TYPE = new ParameterizedTypeReference<List<ActionDescriptionDto>>() {};

    final TypeReference<List<ActionDescriptionDto>> ACTION_DESCRIPTION_DTO_TYPE = new TypeReference<List<ActionDescriptionDto>>() {
    };

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected TestRestTemplate restTemplate;

    @SneakyThrows
    <T> T loadFromJson(String referenceFilename, Class<T> clazz) {
        try (InputStream inputStream = BaseApplicationTest.class.getResourceAsStream(referenceFilename)) {
            return objectMapper.readValue(inputStream, clazz);
        }
    }

    @SneakyThrows
    <T> T loadFromJson(String referenceFilename, TypeReference<T> clazz) {
        try (InputStream inputStream = BaseApplicationTest.class.getResourceAsStream(referenceFilename)) {
            return objectMapper.readValue(inputStream, clazz);
        }
    }

    List<ActionDescriptionDto> filterActionSearchInformation(List<ActionDescriptionDto> actionDescriptionDto, String filter) {
        return actionDescriptionDto.stream().filter(a -> a.getTitle().contains(filter)).collect(Collectors.toList());
    }
    List<ProductDescriptionDto> filterProductSearchInformation(List<ProductDescriptionDto> products, String filter) {
        return products.stream().filter(a -> a.getTitle().contains(filter)).collect(Collectors.toList());
    }
}
