create table action (
    id long auto_increment  primary key,
    name varchar(250) not null,
    count int not null,
);

create table product(
                        id long auto_increment  primary key,
                        name varchar(250) not null,
                        count int not null,
);
