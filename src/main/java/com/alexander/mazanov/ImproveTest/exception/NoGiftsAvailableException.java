package com.alexander.mazanov.ImproveTest.exception;

public class NoGiftsAvailableException extends RuntimeException {
    public NoGiftsAvailableException(String message) {
        super(message);
    }
}
