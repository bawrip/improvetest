package com.alexander.mazanov.ImproveTest.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(NoGiftsAvailableException.class)
    public final ResponseEntity<String> handleNoGiftsAvailableForActionException(NoGiftsAvailableException exc, WebRequest request) {
        log.error("NoGiftsAvailableForAction Exception occurred during operation. " + exc.getMessage());
        return new ResponseEntity<>("Error. " + exc.getMessage() , HttpStatus.NOT_FOUND);
    }
}
