package com.alexander.mazanov.ImproveTest.repository;

import com.alexander.mazanov.ImproveTest.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    List<Product> findByNameContainsAndCountGreaterThan(String name, int count);

}
