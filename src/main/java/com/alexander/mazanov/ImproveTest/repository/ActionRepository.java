package com.alexander.mazanov.ImproveTest.repository;

import com.alexander.mazanov.ImproveTest.entity.Action;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActionRepository extends CrudRepository<Action, Long> {
    List<Action> findByCountGreaterThan(int count);
}
