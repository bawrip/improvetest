package com.alexander.mazanov.ImproveTest.controller;

import com.alexander.mazanov.ImproveTest.dto.action.ActionDescriptionDto;
import com.alexander.mazanov.ImproveTest.dto.action.ActionDto;
import com.alexander.mazanov.ImproveTest.dto.action.CreateActionDto;
import com.alexander.mazanov.ImproveTest.entity.Action;
import com.alexander.mazanov.ImproveTest.service.action.ActionConverter;
import com.alexander.mazanov.ImproveTest.service.action.ActionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ActionController {

    private final ActionService actionService;
    private final ActionConverter actionConverter;

    @GetMapping(value = "/")
    public String getHello() {
        return "Hello World!";
    }

    @GetMapping(value = "/action")
    public List<ActionDescriptionDto> getActionList() {
        return actionService.findAll().stream().map(actionConverter::fromEntity).collect(Collectors.toList());
    }

    @GetMapping(value = "/action/{id}")
    public ActionDto getActionByIdWithProducts(@PathVariable Long id, @RequestParam String filter) {
        return actionService.getActionDtoByActionId(id, filter);
    }

    @PostMapping("/action")
    public Long createAction(@RequestBody @Valid CreateActionDto createActionDto) {
        Action action = actionConverter.fromCreateActionToAction(createActionDto);
        actionService.save(action);

        return action.getId();
    }

    @GetMapping("/action/get/{id}")
    public ActionDescriptionDto getActionById(@PathVariable(name = "id") Long id) {
        return actionConverter.fromEntity(actionService.findById(id));
    }
}
