package com.alexander.mazanov.ImproveTest.controller;

import com.alexander.mazanov.ImproveTest.dto.action.ActionDto;
import com.alexander.mazanov.ImproveTest.dto.product.CreateProductDto;
import com.alexander.mazanov.ImproveTest.dto.product.ProductDescriptionDto;
import com.alexander.mazanov.ImproveTest.entity.Product;
import com.alexander.mazanov.ImproveTest.service.action.ActionService;
import com.alexander.mazanov.ImproveTest.service.action.ProductConverter;
import com.alexander.mazanov.ImproveTest.service.action.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class ProductController {
    private final ActionService actionService;
    private final ProductService productService;
    private final ProductConverter productConverter;

    @GetMapping(value = "/product/{id}")
    public ActionDto decreaseProductCounterByOneAndGetActionById(@PathVariable(name = "id") Long productId, @RequestParam Long actionId) {
        actionService.decreaseActionAndProductCounterByOneAndGetAction(actionId, productId);

        return actionService.getActionDtoByActionId(actionId, "");
    }

    @PostMapping(value = "/product")
    public Long saveProduct(@RequestBody @Validated CreateProductDto createProductDto) {
        Product product = productConverter.fromCreateProductDtoToProduct(createProductDto);
        productService.save(product);

        return product.getId();
    }

    @GetMapping(value = "/product/get/{id}")
    public ProductDescriptionDto getProductById(@PathVariable(name = "id") Long id) {
        return productConverter.fromEntity(productService.findById(id));
    }
}
