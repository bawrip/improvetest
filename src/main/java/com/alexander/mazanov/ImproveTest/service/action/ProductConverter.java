package com.alexander.mazanov.ImproveTest.service.action;

import com.alexander.mazanov.ImproveTest.dto.product.CreateProductDto;
import com.alexander.mazanov.ImproveTest.dto.product.ProductDescriptionDto;
import com.alexander.mazanov.ImproveTest.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface ProductConverter {

    @Mappings({
            @Mapping(target = "title", source = "name"),
            @Mapping(target = "number", source = "count")
    })
    ProductDescriptionDto fromEntity(Product product);

    Product fromCreateProductDtoToProduct(CreateProductDto createProductDto);
}
