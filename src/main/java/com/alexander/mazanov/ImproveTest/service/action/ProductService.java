package com.alexander.mazanov.ImproveTest.service.action;

import com.alexander.mazanov.ImproveTest.entity.Product;
import com.alexander.mazanov.ImproveTest.exception.NoGiftsAvailableException;
import com.alexander.mazanov.ImproveTest.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    List<Product> findByNameContains(String name) {
        return productRepository.findByNameContainsAndCountGreaterThan(name, 0);
    }

    public Product findById(Long id) {
        return productRepository.findById(id).get();
    }

    public void save(Product product) {
        productRepository.save(product);
    }

    void decreaseProductCounterByOne(Long id) {
        Product product = this.findById(id);

        int count = product.getCount();
        if (count > 0) {
            product.setCount(--count);
        } else {
            throw new NoGiftsAvailableException("No gifts of this kind");
        }

        this.save(product);
    }
}
