package com.alexander.mazanov.ImproveTest.service.action;

import com.alexander.mazanov.ImproveTest.dto.action.ActionDto;
import com.alexander.mazanov.ImproveTest.dto.product.ProductDescriptionDto;
import com.alexander.mazanov.ImproveTest.entity.Action;
import com.alexander.mazanov.ImproveTest.exception.NoGiftsAvailableException;
import com.alexander.mazanov.ImproveTest.repository.ActionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ActionService {
    private final ActionRepository actionRepository;
    private final ProductService productService;
    private final ActionConverter actionConverter;
    private final ProductConverter productConverter;

    public List<Action> findAll() {
        return actionRepository.findByCountGreaterThan(0);
    }

    public Action findById(Long id) {
        return actionRepository.findById(id).get();
    }

    private void decreaseActionCounterByOne(Long id) {
        Action action = this.findById(id);
        int actionCount = action.getCount();

        if (actionCount > 0) {
            action.setCount(--actionCount);
        } else {
            throw new NoGiftsAvailableException("No gifts for this promotion");
        }

        this.save(action);
    }

    public void save(Action action) {
        actionRepository.save(action);
    }

    @Transactional(rollbackFor = {NoGiftsAvailableException.class})
    public void decreaseActionAndProductCounterByOneAndGetAction(Long actionId, Long productId) {
        this.decreaseActionCounterByOne(actionId);
        productService.decreaseProductCounterByOne(productId);
    }

    public ActionDto getActionDtoByActionId(Long actionId, String filter) {
        Action action = this.findById(actionId);
        List<ProductDescriptionDto> products = productService.findByNameContains(filter)
                .stream().map(productConverter::fromEntity).collect(Collectors.toList());

        return new ActionDto(actionConverter.fromEntity(action), products);
    }
}
