package com.alexander.mazanov.ImproveTest.service.action;

import com.alexander.mazanov.ImproveTest.dto.action.ActionDescriptionDto;
import com.alexander.mazanov.ImproveTest.dto.action.CreateActionDto;
import com.alexander.mazanov.ImproveTest.entity.Action;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface ActionConverter {

    @Mappings({
            @Mapping(target = "title", source = "name"),
            @Mapping(target = "number", source = "count")
    })
    ActionDescriptionDto fromEntity(Action action);

    Action fromCreateActionToAction(CreateActionDto action);
}
