package com.alexander.mazanov.ImproveTest.dto.action;

import com.alexander.mazanov.ImproveTest.dto.product.ProductDescriptionDto;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ActionDto {
    private ActionDescriptionDto action;
    private List<ProductDescriptionDto> products;
}
