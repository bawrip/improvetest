package com.alexander.mazanov.ImproveTest.dto.product;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class CreateProductDto {
    @NotEmpty(message = "Name may not be empty")
    private String name;
    private int count;
}
