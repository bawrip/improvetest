package com.alexander.mazanov.ImproveTest.dto.product;

import lombok.Data;

@Data
public class ProductDescriptionDto {
    private long id;
    private String title;
    private int number;
}
