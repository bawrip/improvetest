package com.alexander.mazanov.ImproveTest.dto.action;

import lombok.Data;

@Data
public class ActionDescriptionDto {
    private long id;
    private String title;
    private long number;
}
