package com.alexander.mazanov.ImproveTest.dto.action;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class CreateActionDto {
    @NotEmpty(message = "Name may not be empty")
    private String name;
    private int count;
}
