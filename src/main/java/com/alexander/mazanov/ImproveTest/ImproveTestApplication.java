package com.alexander.mazanov.ImproveTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImproveTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImproveTestApplication.class, args);
	}

}
